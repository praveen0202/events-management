import {
  FETCH_EVENTS_REQUEST,
  FETCH_EVENTS_SUCCESS,
  FETCH_EVENTS_FAILURE
} from '../constants';

export const fetchEventsRequest = {
  type: FETCH_EVENTS_REQUEST
};

export const fetchEventsSuccess = (payload) => ({
  type: FETCH_EVENTS_SUCCESS,
  payload
});

export const fetchEventsFailure = {
  type: FETCH_EVENTS_FAILURE
}
