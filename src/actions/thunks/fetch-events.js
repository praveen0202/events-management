import axios from 'axios';

import { fetchEventsRequest, fetchEventsSuccess, fetchEventsFailure } from '../events';
import { EVENTS_API } from '../../constants';

export const fetchEvents = () => async dispatch => {
  dispatch(fetchEventsRequest);
  console.log("API call")
  await axios.get(EVENTS_API, {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer `
    },
  }).then((response) => {
    dispatch(fetchEventsSuccess(response));
  }).catch((error) => {
    dispatch(fetchEventsFailure);
  })
};
