import axios from 'axios';

import { signupRequest, signupUserSuccess, signupUserFailure } from '../users';
import { SIGNUP_API } from '../../constants';

export const createUser = (payload, navigate, notificationApi) => async dispatch => {
  dispatch(signupRequest);
  await axios.post(SIGNUP_API, {user: payload}, {
    headers: {
      "Content-Type": "application/json"
    }
  }).then((response) => {
    dispatch(signupUserSuccess);
    notificationApi.success({message: "Signup successful.", placement: 'bottomRight'});
    navigate('/login');
  }).catch((error) => {
    dispatch(signupUserFailure);
    notificationApi.error({message: error, placement: 'bottomRight'});
  })
};
