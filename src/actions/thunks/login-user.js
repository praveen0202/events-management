import axios from 'axios';
import { wrapper } from 'axios-cookiejar-support';
import { CookieJar } from 'tough-cookie';

import { loginRequest, loginUserSuccess, loginUserFailure } from '../users';
import { LOGIN_API } from '../../constants';

const jar = new CookieJar();
const client = wrapper(axios.create({ jar }));

export const loginUser = (payload, navigate) => async dispatch => {
  dispatch(loginRequest);
  await client.post(LOGIN_API, {user: payload}, {
    headers: {
      "Content-Type": "application/json"
    }
  }).then((response) => {
    console.log(response.config.jar)
    dispatch(loginUserSuccess);
    localStorage.setItem('authed', true);
    navigate('/events');
  }).catch((error) => {
    dispatch(loginUserFailure);
    localStorage.setItem('authed', false);
  })
};
