import {
  SIGNUP_USER_REQUEST,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAILURE,
  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  CLEAR_LOGIN_USER,
} from '../constants';

export const signupRequest = {
  type: SIGNUP_USER_REQUEST
};

export const signupUserSuccess = {
  type: SIGNUP_USER_SUCCESS
};

export const signupUserFailure = {
  type: SIGNUP_USER_FAILURE
}

export const loginRequest = {
  type: LOGIN_USER_REQUEST
};

export const loginUserSuccess = {
  type: LOGIN_USER_SUCCESS
};

export const loginUserFailure = {
  type: LOGIN_USER_FAILURE
}
