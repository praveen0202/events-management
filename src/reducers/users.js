import {
  SIGNUP_USER_REQUEST,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAILURE,
  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  CLEAR_LOGIN_USER,
} from '../constants';

const initialState = {
  user: undefined,
  apiRequestInProgress: false,
}

const events = (state = initialState, { type, payload }) => {
  switch (type) {
    case SIGNUP_USER_REQUEST:
    case LOGIN_USER_REQUEST:
      return {
        user: undefined,
        apiRequestInProgress: true
      };
    case SIGNUP_USER_SUCCESS:
    case LOGIN_USER_SUCCESS:
      return {
        user: payload,
        apiRequestInProgress: false
      };
    case SIGNUP_USER_FAILURE:
    case LOGIN_USER_FAILURE:
      return {
        apiRequestInProgress: false
      };
    case CLEAR_LOGIN_USER:
      return initialState;
    default:
      return state
  }
}
    
export default events;
  