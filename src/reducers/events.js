import {
  FETCH_EVENTS_REQUEST,
  FETCH_EVENTS_SUCCESS,
  FETCH_EVENTS_FAILURE,
  ADD_EVENT_REQUEST,
  ADD_EVENT_SUCCESS,
  ADD_EVENT_FAILURE,
  UPDATE_EVENT_REQUEST,
  UPDATE_EVENT_SUCCESS,
  UPDATE_EVENT_FAILURE,
  DELETE_EVENT_REQUEST,
  DELETE_EVENT_SUCCESS,
  DELETE_EVENT_FAILURE,
  CLEAR_EVENTS,
} from '../constants';

const initialState = {
  events: [],
  apiRequestInProgress: false,
}

const events = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_EVENTS_REQUEST:
      return {
        events: [],
        apiRequestInProgress: true
      };
    case FETCH_EVENTS_SUCCESS:
      return {
        events: payload,
        apiRequestInProgress: false
      };
    case ADD_EVENT_SUCCESS:
      return {
        events: [
          ...state.events,
          { ...payload }
        ],
        apiRequestInProgress: false
      };
    case UPDATE_EVENT_SUCCESS:
      const eventIndex = state.events.filter(event => event.id === payload.id);
      const newEvents = state.events;
      newEvents[eventIndex] = payload;
      return {
        events: newEvents,
        apiRequestInProgress: false
      };
    case DELETE_EVENT_SUCCESS:
      const events = state.events.filter(event => event.id !== payload.id);
      return {
        events,
        apiRequestInProgress: false
      };
    case ADD_EVENT_REQUEST:
    case UPDATE_EVENT_REQUEST:
    case DELETE_EVENT_REQUEST:
      return {
        apiRequestInProgress: true
      }; 
    case FETCH_EVENTS_FAILURE:
    case ADD_EVENT_FAILURE:
    case UPDATE_EVENT_FAILURE:
    case DELETE_EVENT_FAILURE:
      return {
        apiRequestInProgress: false
      }
    case CLEAR_EVENTS:
      return initialState;
    default:
      return state
  }
}
  
export default events;
