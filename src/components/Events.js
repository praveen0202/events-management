import React, { useEffect } from 'react';
import { Button, Table, Col, Row  } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux'

import { fetchEvents } from '../actions/thunks/fetch-events';

const AddEventButton = styled(Button)`
  float: right;
`;

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    render: (text) => <a>{text}</a>,
  },
  {
    title: 'Date',
    dataIndex: 'date',
  },
  {
    title: 'Description',
    dataIndex: 'description',
  },
  {
    title: 'Price',
    dataIndex: 'price',
  },
  {
    title: 'Booking Type',
    dataIndex: 'bookingType',
  },
];

const data = [];

const Events = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchEvents());
  }, [])

  const onCreateEventClick = () => {
    navigate('/create-event');
  };

  return (
    <>
      <Row>
        <Col span={16} flex={1}><h2>Events</h2></Col>
        <Col span={8}>
          <AddEventButton type="primary" shape="round" icon={<PlusOutlined />} size='large' onClick={onCreateEventClick}>
            Create Event
          </AddEventButton>
        </Col>
      </Row>
      <Table
        rowSelection={{
            type: 'checkbox',
        }}
        columns={columns}
        dataSource={data}
      />
    </>
  );
};

export default Events;
