import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import AppLayout from './AppLayout';
import Events from './Events';
import CreateEventForm from './CreateEventForm';
import LoginForm from './LoginForm';
import Signup from './Signup';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<RequiredAuth><AppLayout/></RequiredAuth>}>
          <Route index element={<RequiredAuth><Events/></RequiredAuth>} />
          <Route path="/create-event" element={<RequiredAuth><CreateEventForm/></RequiredAuth>} />
          <Route path="/events" element={<RequiredAuth><Events/></RequiredAuth>} />
        </Route>
        <Route path="/login" element={<LoginForm />} />
        <Route path="/signup" element={<Signup />} />
      </Routes>
    </BrowserRouter>
  );
};

const RequiredAuth = ({ children }) => {
  const authed = localStorage.getItem('authed');

  if(authed === "true") {
    return children;
  } else {
    return <LoginForm />
  }
}

export default App;
