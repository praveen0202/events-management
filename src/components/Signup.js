import React from 'react';
import { Form, Input, Button, notification } from 'antd';
import { MailOutlined, UserOutlined, LockOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { useDispatch } from 'react-redux'
import { useNavigate } from "react-router-dom";

import LoginLayout from './LoginLayout';
import { createUser } from '../actions/thunks/create-user';

const StyledSpan = styled.span`
  margin-left: 20px;
`;

const Signup = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [api, contextHolder] = notification.useNotification();

  const onFinish = values => {
    dispatch(createUser(values, navigate, api));
  };

  return (
    <LoginLayout>
      <h2>Signup</h2>
      <Form
        name="signup-form"
        className="signup-form"
        initialValues={{}}
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              pattern: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              message: 'Email is not valid'
            },
          ]}
        >
          <Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
        </Form.Item>
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
              message: 'Password should contain minimum 8 characters with at least 1 Uppercase and 1 special character',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="signup-form-button">
            Sign up
          </Button>
          <StyledSpan>Or <a href="/login">Login</a></StyledSpan>
        </Form.Item>
      </Form>
    </LoginLayout>
  );
};

export default Signup;
