import React, { useState } from 'react';
import { CreditCardOutlined, FileOutlined, LoginOutlined, LogoutOutlined, ScheduleTwoTone } from '@ant-design/icons';
import { Layout, Menu, theme } from 'antd';
import { Outlet, useNavigate } from "react-router-dom";

const { Content, Footer, Sider } = Layout;

function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}

const items = [
  getItem('Events', 'events', <CreditCardOutlined />),
  getItem('Create Event', 'create-event', <FileOutlined />),
  getItem('Logout', 'logout', <LogoutOutlined />),
  getItem('Login', 'login', <LogoutOutlined />),
  getItem('Signup', 'signup', <LoginOutlined />),
];

const appItems = [
  getItem('Events', '1', <ScheduleTwoTone />),
];

const AppLayout = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const navigate = useNavigate();

  const onMenuItemClick = ({ item, key, keyPath }) => {
    navigate(key);
  };

  return (
    <Layout
      style={{
        minHeight: '100vh',
      }}
    >
      <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
        <div
          style={{
            height: 32,
            margin: 16,
            background: 'rgba(255, 255, 255, 0.2)',
            color: 'white'
          }}
        ><Menu theme="dark" mode="inline" items={appItems} /></div>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} onClick={onMenuItemClick} />
      </Sider>
      <Layout className="site-layout">
        <Content
          style={{
            margin: '0 16px',
          }}
        >
          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: colorBgContainer,
            }}
          >
            <Outlet />
          </div>
        </Content>
        <Footer
          style={{
            textAlign: 'center',
          }}
        >
          Ant Design ©2023 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
};

export default AppLayout;
