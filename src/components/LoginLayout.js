import React from 'react';
import styled from 'styled-components';
import { Layout } from 'antd';

const { Header } = Layout;

const ParentDiv = styled.div`
  height: 100vh;
  width: 100%;
  background: aliceblue;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ChildDiv = styled.div`
  background-color: white;
  min-width: 500px;
  padding: 20px;
`;

const StyledH2 = styled.h2`
  margin: 0;
`;

const headerStyle = {
  color: '#fff',
  height: 64,
  paddingInline: 50,
  lineHeight: '64px',
  backgroundColor: '#7dbcea',
};

const LoginLayout = ({ children }) => {
  return (
    <Layout>
      <Header style={headerStyle}><StyledH2>Events Management</StyledH2></Header>
      <ParentDiv>
        <ChildDiv>
          {children}
        </ChildDiv>
      </ParentDiv>
    </Layout>
  );
};

export default LoginLayout;
